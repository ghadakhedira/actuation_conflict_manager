var exports = module.exports = {};

var SoftwareComponent = require("../acm-metamodel/SoftwareComponent.js");
var Link = require("../acm-metamodel/Link.js");
var Metamodel = require("../acm-metamodel/Metamodel.js");


var str = "@startuml "+"\n" +
"caption Instances and Connectors in configuration SimpleBuzzerArduino"+"\n" +
"component [simple_buzzer : SimpleBuzzer]<<PIM>>"+"\n" +
"component [buzzer : Buzzer]<<PIM>>"+"\n" +
"component [timer : TimerArduino]<<PSM>>"+"\n" +
"component [digital_output : DigitalOutputArduino]<<PIM>>"+"\n" +
"component [arduino : ArduinoArduino]<<PSM>>"+"\n" +
"[buzzer : Buzzer] -(0- [timer : TimerArduino] : timer => timer"+"\n" +
"[simple_buzzer : SimpleBuzzer] -(0- [buzzer : Buzzer] : Buzzer => Buzzer"+"\n" +
"[buzzer : Buzzer] -(0- [digital_output : DigitalOutputArduino] : DigitalOutput => DigitalOutput"+"\n" +
"[digital_output : DigitalOutputArduino] -(0- [arduino : ArduinoArduino] : DigitalIO => DigitalIO"+"\n" +
"[simple_buzzer : SimpleBuzzer] -(0- [timer : TimerArduino] : timer => timer"+"\n" +
"@enduml;"+"\n";

function fromContent(fileContent){
    var softwarecomponents = [];
    var links = [];
    var lines = fileContent.split("\n");
    var index = 2;
    var words = lines[index].split(" ");
    while(words[0] == "component"){
        var name = words[1].slice(1);
        var componentType = "softwareComponent";
        var id_parent = "id_parent";        
        softwarecomponents.push(new SoftwareComponent(softwarecomponents.length,name,0,0,componentType,id_parent));
        //console.log(name);
        words = lines[++index].split(" ");
    }
    while(index<lines.length-2){
        words = lines[index].split(" ");  
        var name = words[0].slice(1) + "-"+words[4].slice(1);
        var from;
        var to;        
        for(var i=0;i<softwarecomponents.length;i++){
            if(softwarecomponents[i].name == words[0].slice(1)){
                from = softwarecomponents[i];
            }
            if(softwarecomponents[i].name == words[4].slice(1)){
                to = softwarecomponents[i];
            }
        }
        
        links.push(new Link(links.length,name,0,0,from,to));
        index++;
    }
    console.log(links)
    var metamodel = new Metamodel(softwarecomponents,links);
}

//fromContent(str);