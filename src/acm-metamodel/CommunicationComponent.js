var SoftwareComponent = require("./SoftwareComponent.js");


class CommunicationComponent extends SoftwareComponent {
    constructor(id, name, x, y, type, id_parent,isOutNode) {
        super(id, name, x, y, type, id_parent);
        this.isOutNode = isOutNode
        
    }

    //to change
    buildNodeRedJson() {
        var json = {};
        json.id = this.id;
        json.name = this.name;
        json.type = this.type;
        //json.z = this.id_parent;
        json.x = this.x;
        json.y = this.y;
        json.fieldType = "msg";
        json.format = "handlebars";
        json.syntax = "mustache";
        json.template = "This is the payload: {{payload}} !";
        json.output = "str";

        json.wires = [[]];
        return json;
    }
}

module.exports = CommunicationComponent;
