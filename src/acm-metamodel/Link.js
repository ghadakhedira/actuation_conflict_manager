var ACMElement = require("./ACMElement.js");
var SoftwareComponent = require("./SoftwareComponent.js");

class Link extends ACMElement {
    constructor(id, name, x, y, from, to, port = 0) {
        super(id, name,x,y);
        this.from = from;
        this.to = to;
        this.port = port;
    }
    
}

module.exports = Link;
